/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package xogame;
import java.util.Scanner;
/**
 *
 * @author Lenovo
 */
public class XOGame {

    /**
     * @param args the command line arguments
     */
    
    private char[][] board;
    
    public void printBoard() {
        System.out.println("|-----------|");
        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " | ");
            }
            System.out.println();
            System.out.println("|-----------|");
        }
    }
    
    public void newBoard() {
        board = new char[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = '-';
            }
        }
    }
    
    public boolean checkDraw(){
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if(board[i][j] == '-'){
                    return false;
                }
            }
        }
        return true;
    }
    
    public boolean checkWin(char player){
        //check row
        for(int i=0; i<3; i++){
            if(board[i][0] == player && board[i][1]== player&& board[i][2]== player){
                return true;
            }
        }
        
        //check col
        for(int i=0; i<3; i++){
            if(board[0][i]== player&&board[1][i]== player && board[2][i]== player ){
                return true;
            }
        }
        
        //check diagonals
        if(board[0][0]== player&&board[1][1]== player && board[2][2]== player){
            return true;     
        }
        else if(board[0][2]== player&&board[1][1]== player && board[2][0]== player){
            return true;  
        }
        return false;
    }
    
    public void play() {
    Scanner kb = new Scanner(System.in);
    newBoard();

    System.out.println("X--------------------X");
    System.out.println("O Welcome To OX Game O");
    System.out.println("X--------------------X");

    System.out.print("Player 1, choose 'X' or 'O': ");
    char player1 = kb.nextLine().toUpperCase().charAt(0);

    while (player1 != 'X' && player1 != 'O') {
        System.out.print("Invalid choice. Please choose 'X' or 'O': ");
        player1 = kb.nextLine().toUpperCase().charAt(0);
    }

    char player2 = (player1 == 'X') ? 'O' : 'X';

    System.out.println("Player 1: " + player1);
    System.out.println("Player 2: " + player2);

    char currentPlayer = player1;
    boolean gameEnd = false;
    printBoard();

    while (!gameEnd) {
        System.out.println("It's player " + currentPlayer + "'s turn.");
        System.out.println("Please input row and column [ex. 1 1]:");
        int row = kb.nextInt() - 1;
        int col = kb.nextInt() - 1;

        if (row >= 0 && row < 3 && col >= 0 && col < 3 && board[row][col] == '-') {
            board[row][col] = currentPlayer;
            printBoard();
            if (checkWin(currentPlayer)) {
                System.out.println( currentPlayer + " win!!!");
                gameEnd = true;
            } else if (checkDraw()) {
                System.out.println("The game is a draw!");
                gameEnd = true;
            } else {
                currentPlayer = (currentPlayer == player1) ? player2 : player1;
            }
        } else {
            System.out.println("Invalid move. Please try again.");
        }
    }

    System.out.print("Do you want to play again? (y/n): ");
    String newGame = kb.next();
    if (newGame.equalsIgnoreCase("y")) {
        play();
    } else {
        System.out.println("Thank you for playing! Goodbye!");
    }
}
    public static void main(String[] args) {
        XOGame game = new XOGame();
        game.play();
    }
       
    
}
